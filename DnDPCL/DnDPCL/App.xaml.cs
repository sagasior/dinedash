﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace DnDPCL
{
    public partial class App : Application 
    {
        public App()
        {
            InitializeComponent();
            //MainPage = new HomePage();
            try
            {
                var nav = new NavigationPage(new LoginPage());
                MainPage = nav;
            }
            catch(Exception e)
            {

            }      
            
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
