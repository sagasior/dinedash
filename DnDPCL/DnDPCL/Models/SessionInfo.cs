﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnDPCL.Models
{
    public class SessionInfo
    {
        private List<DishModel> _orderedItems;
        
        public List<DishModel> OrderedItem
        {
            get { return _orderedItems; }
        }

        public SessionInfo()
        {
            _orderedItems = new List<DishModel>();

        }


    }
}
