﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DnDPCL.Models
{
    public class DishModel : ModelBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public Image DisplayImage { get; set; }
        private string _imageURI { get; set; }
        public ImageSource ImageSourceInfo { get; set; }
        private ObservableCollection<Ingredient> _ingredientList;
        private ObservableCollection<DietRestrictions> _dishRestrictions;
        private ObservableCollection<OrderConfigurationModel> _configurationModel;
        private ObservableCollection<ObservableCollection<OrderConfigurationModel>> _sideOrderItems;
               

        //set from custom profile
        public bool RestrictionFlag { get; set; }
        public string RestrictionString { get; set; }
        public int _quantity;

        public double PriceSum
        {
            get
            {
                double sideSum = 0;
                foreach(ObservableCollection<OrderConfigurationModel> o in _sideOrderItems)
                {
                    foreach(OrderConfigurationModel m in o)
                    {
                        if(m.SelectedModel != null)
                        {
                           sideSum += m.SelectedModel.Price;
                        }                       
                    }
                }

                return sideSum + (Price * Quantity);
            }
        }

        public string ImageURI
        {
            get
            {
                return _imageURI;
            }
            set
            {
                _imageURI = value;
                ImageSourceInfo = ImageSource.FromResource(_imageURI);
            }
        }

        public int Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
                OnPropertyChanged("Quantity");                
            }            
        }

        public ObservableCollection<ObservableCollection<OrderConfigurationModel>> SideOrderItems
        {
            get { return _sideOrderItems; }
            set { _sideOrderItems = value; }
        }


        public ObservableCollection<OrderConfigurationModel> ConfigurationModel
        {
            get { return _configurationModel; }
            set { _configurationModel = value; }
        }

        public ObservableCollection<Ingredient> Ingredients
        {
            get { return _ingredientList; }     
            set { _ingredientList = value; }       
        }

        public ObservableCollection<DietRestrictions> DishRestrictions
        {
            get { return _dishRestrictions; }
            set { _dishRestrictions = value; }
        }

        public string PriceString
        {
            get { return string.Concat("$", Price.ToString()); }
        }

        public DishModel()
        {
            ImageURI = "";
            Name = "";
            Description = "";
            RestrictionFlag = false;
            RestrictionString = "";
            DisplayImage = new Image();
            _ingredientList = new ObservableCollection<Ingredient>();
            _dishRestrictions = new ObservableCollection<DietRestrictions>();
            _configurationModel = new ObservableCollection<OrderConfigurationModel>();
            _sideOrderItems = new ObservableCollection<ObservableCollection<OrderConfigurationModel>>();         
            //Image img = new Image();
            //ImageSourceInfo = img.Source;         
        }    

    }
}
