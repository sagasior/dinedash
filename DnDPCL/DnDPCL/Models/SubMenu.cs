﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnDPCL.Models
{
    public class SubMenu
    {
        private ObservableCollection<DishModel> _dishs;
        public string Title { get; set; }

        public ObservableCollection<DishModel> Dishs
        {
            get { return _dishs; }
            set { _dishs = value; }
        }

        public SubMenu()
        {
            Title = "";
            _dishs = new ObservableCollection<DishModel>();
        }

    }
}
