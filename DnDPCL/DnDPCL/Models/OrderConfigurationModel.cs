﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnDPCL.Models
{
    public class OrderConfigurationModel
    {
        public string Title { get; set; }
        ObservableCollection<SideOrderModel> _subMenuItems;
        public SideOrderModel SelectedModel { get; set; }

        public ObservableCollection<SideOrderModel> SubMenuItems
        {
            get { return _subMenuItems;}
            set { _subMenuItems = value; }
        }
        
        public OrderConfigurationModel()
        {
            _subMenuItems = new ObservableCollection<SideOrderModel>();
        }
    }
}
