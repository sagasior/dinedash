﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnDPCL.Models
{
    public class User
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string CardNumber { get; set; }
        public string Password { get; set; }

        public string DietRestriction { get; set; }
        public ObservableCollection<Ingredient> IngerdientRestrictions { get; set; }
        public bool IsAuthenticated { get; set; }

        public User()
        {
            IngerdientRestrictions = new ObservableCollection<Ingredient>();
            Initalize();
                        
        }

        private void Initalize()
        {
            Name = "";
            Email = "";
            CardNumber = "";
            Password = "";
            DietRestriction = "";
            IsAuthenticated = false;
        }

    }
}
