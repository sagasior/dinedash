﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnDPCL.Models
{
    public class SideOrderModel
    {
        public double Price { get; set; }
        public string Description { get; set; }
        public bool Selected { get; set; }

        public SideOrderModel()
        {

        }

        public SideOrderModel(string description, double price)
        {
            Price = price;
            Description = description;
        }   

        public override string ToString()
        { 
            if(Price > 0)
            {
                return string.Concat(Description, " + $", Price);
            }

            return Description;

        }
    }
}
