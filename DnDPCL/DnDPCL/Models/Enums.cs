﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnDPCL.Models
{
   public enum DietRestrictions
    {
        None,
        Vegan,
        Vegetarian,
        Islamic,
        Kosher,
        GlutenFree,
    }
}
