﻿using DnDPCL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DnDPCL
{
    public partial class NewUserPage : ContentPage
    {
        private User LoggedUser;

        private string _selectedDiet;
        public string _name { get; set; }
        public string _email { get; set; }
        public string _cardNumber { get; set; }
        public string _password { get; set; }
       
        public ObservableCollection<Ingredient> IngredientRestrictions { get; set; }

        public string SelectedDiet
        {
            get { return _selectedDiet; }
            set { _selectedDiet = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public string CardNumber
        {
            get{ return _cardNumber;}
            set { _cardNumber = value; }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public NewUserPage(User user)
        {
            InitializeComponent();

            LoggedUser = user;            
            InitalizeProfile();
            //make shallow copy
            //IngredientRestrictions = new ObservableCollection<Ingredient>(LoggedUser.IngerdientRestrictions);
            this.BindingContext = this;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();            
        }

        void AddItemClicked(object sender, EventArgs e)
        {
            IngredientRestrictions.Add(new Ingredient());
        }

        void InitalizeProfile()
        {
            Name = LoggedUser.Name;
            Email = LoggedUser.Email;
            SelectedDiet = LoggedUser.DietRestriction;
            CardNumber = LoggedUser.CardNumber =
            Password = LoggedUser.Password;
            IngredientRestrictions = LoggedUser.IngerdientRestrictions;           
        }

        void SavedProfileSaved(object sender, EventArgs e)
        {
            LoggedUser.Name = Name;
            LoggedUser.Email = Email;
            LoggedUser.DietRestriction = SelectedDiet;
            LoggedUser.CardNumber = CardNumber;
            LoggedUser.Password = Password;
            LoggedUser.IngerdientRestrictions = new ObservableCollection<Ingredient>(IngredientRestrictions.ToList());
            LoggedUser.IsAuthenticated = true;

            //go back to main screen
            Navigation.PopAsync();
        }

    }
}
