﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DnDPCL
{
    public partial class TablePage : ContentPage
    {
        public string TableCode { get; set; }

        public TablePage()
        {
            InitializeComponent();
        }

        void BtnSubmitClicked(object sender, EventArgs e)
        {
            TabbedMenuScreen mnu = new TabbedMenuScreen();
            Navigation.PushAsync(mnu);
        }

    }
}
