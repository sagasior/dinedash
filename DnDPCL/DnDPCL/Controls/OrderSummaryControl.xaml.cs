﻿using DnDPCL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DnDPCL.Controls
{
    public partial class OrderSummaryControl : StackLayout
    {
        private DishModel _dm;

        private int titleMult = 7;
        private int indent = 15;

        private int headerSize = 12;
        private int subHeaderSize = 10;

        public OrderSummaryControl(DishModel dm)
        {
            InitializeComponent();
            _dm = dm;
            configurePanel();
        }
                
        private void configurePanel()
        {   
            if(_dm.SideOrderItems.Count > 0)
            {
                foreach (ObservableCollection<OrderConfigurationModel> d in _dm.SideOrderItems)
                {
                    this.Children.Add(CreateHeader(_dm.Name, _dm.Price));

                    foreach (OrderConfigurationModel o in d)
                    {
                        if (o.SelectedModel != null)
                        {
                            this.Children.Add(CreateSubHeader(o.SelectedModel.Description, o.SelectedModel.Price));
                        }
                    }
                }
            }     
            else
            {
                //add items with no customization
                for(int i =0; i < _dm.Quantity; i++)
                {
                    this.Children.Add(CreateHeader(_dm.Name, _dm.Price));
                }                
            }   
           
        }        

        private Grid CreateHeader(string title, double price)
        {
            Grid gr = new Grid();
            gr.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });

            gr.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(titleMult, GridUnitType.Star) });
            gr.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            gr.Children.Add(new Label { Text = title, FontSize = headerSize }, 0, 1);
            gr.Children.Add(new Label { Text = String.Concat("$", price.ToString("0.##")), FontSize = headerSize }, 1, 1);

            return gr;
        }

        private Grid CreateSubHeader(string title, double price)
        {
            Grid gr = new Grid();
            gr.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });

            gr.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(indent) });
            gr.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(titleMult, GridUnitType.Star) });
            gr.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            gr.Children.Add(new Label { Text = title, FontSize = subHeaderSize }, 1, 1);
            gr.Children.Add(new Label { Text = String.Concat("$", price.ToString("0.##")), FontSize = subHeaderSize }, 2, 1);

            return gr;
        }      
        
        public static Grid CreateSubTotalGrid(string title, double price)
        {
            Grid gr = new Grid();
            gr.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });

            gr.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            gr.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            gr.Children.Add(new Label { Text = title, FontSize = 12, TextColor = Color.Red }, 0, 1);
            gr.Children.Add(new Label { Text = String.Concat("$", price.ToString("0.##")), FontSize = 12, TextColor = Color.Red, HorizontalOptions = LayoutOptions.End }, 1, 1);

            return gr;

        }  

    }
}
