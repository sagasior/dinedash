﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DnDPCL.Controls
{
    public partial class TextBindingStackLayout : StackLayout
    {
        IEnumerable<string> _strList;
        int _fontSize;

        public TextBindingStackLayout(string Title, IEnumerable<string> strings, int fontSize)
        {
            _strList = strings;
            _fontSize = fontSize;   
            InitializeComponent();

            this.Children.Add(GetTitleLabel(Title));
            stackEmUp();
        }

        private void stackEmUp()
        {          

            foreach(string s in _strList)
            {
                Label lbl = GatLabel(s);
                this.Children.Add(lbl);
            }
        }

        private Label GatLabel(string s)
        {
            Label l = new Label();
            l.Text = s;
            l.FontSize = _fontSize;
            return l;
        }

        private Label GetTitleLabel(string s)
        {
            Label l = new Label();
            l.Text = s;
            l.FontSize = _fontSize*1.2;
            return l;
        }

    }
}
