﻿using DnDPCL.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DnDPCL.Controls
{
    public partial class OrderItemSubMenu : StackLayout
    {

        public ObservableCollection<OrderConfigurationModel> subItems { get; set; }

        //public static BindableProperty ItemsSourceProperty =
        //   BindableProperty.Create<OrderItemSubMenu, ObservableCollection<OrderConfigurationModel>>(o => o.ItemsSource, default(ObservableCollection<OrderConfigurationModel>), propertyChanged: OnItemsSourceChanged);

        public OrderItemSubMenu()
        {
            //may not be needed
            InitializeComponent();
        }

        //public ObservableCollection<OrderConfigurationModel> ItemsSource
        //{
        //    get { return (ObservableCollection<OrderConfigurationModel>)GetValue(ItemsSourceProperty); }
        //    set { SetValue(ItemsSourceProperty, value); }
        //}

        //private static void OnItemsSourceChanged(BindableObject bindable, IList oldvalue, IList newvalue)
        //{
        //    var picker = bindable as OrderItemSubMenu;

        //    if (picker != null)
        //    {
        //        picker.Children.Clear();

        //        if (newvalue == null) return;

        //        foreach (var item in newvalue)
        //        {
        //            if (item is OrderConfigurationModel)
        //            {
        //                OrderConfigurationModel o = item as OrderConfigurationModel;

        //                BindablePickerControl cnt = new BindablePickerControl()
        //                {
        //                    ItemsSource = o.SubMenuItems,                            
        //                    Title = o.Title,
        //                    HorizontalOptions = LayoutOptions.Center
        //                };
        //                o.SelectedModel = cnt._hardRef as SideOrderModel;
                                               
        //                picker.Children.Add(cnt);
        //            }                   
        //        }

        //    }
        //}

        public OrderItemSubMenu(ObservableCollection<OrderConfigurationModel> orderModel, string OrderName = "0")
        {  
            InitializeComponent();
            subItems = orderModel;
            AddSubMenu(orderModel, OrderName);
        }

        public void AddSubMenu(ObservableCollection<OrderConfigurationModel> orderModel, string orderName)
        {
            this.Children.Add(new Label() { Text =  orderName});
            foreach(OrderConfigurationModel o in orderModel)
            {
                BindablePickerControl cnt = new BindablePickerControl(o)
                {
                    ItemsSource = o.SubMenuItems,                    
                    Title = o.Title,
                    HorizontalOptions = LayoutOptions.Center
                };    
                

                this.Children.Add(cnt);
            }
        }

         

    }
}
