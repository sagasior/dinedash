﻿using DnDPCL.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DnDPCL.Controls
{
    public partial class BindablePickerControl : Picker
    {

        private OrderConfigurationModel _hardRef;

        public BindablePickerControl()
        {
            //InitializeComponent();
            //this.SelectedIndexChanged += OnSelectedIndexChanged;
            Init();
        }

        public BindablePickerControl(OrderConfigurationModel hardRef)
        {
            //InitializeComponent();
            //this.SelectedIndexChanged += OnSelectedIndexChanged;
            _hardRef = hardRef;
            Init();
        }

        private void Init()
        {
            InitializeComponent();
            this.SelectedIndexChanged += OnSelectedIndexChanged;
        }

        public static BindableProperty ItemsSourceProperty =
            BindableProperty.Create<BindablePickerControl, IList>(o => o.ItemsSource, default(IList), propertyChanged: OnItemsSourceChanged);

        public static BindableProperty SelectedItemProperty =
            BindableProperty.Create<BindablePickerControl, object>(o => o.SelectedItem, default(object), propertyChanged: UpdateSelected);


        public string DisplayMember { get; set; }

        public IList ItemsSource
        {
            get { return (IList)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public object SelectedItem
        {
            get { return (object)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }          

        private static void OnItemsSourceChanged(BindableObject bindable, IList oldvalue, IList newvalue)
        {
            var picker = bindable as BindablePickerControl;

            if (picker != null)
            {
                picker.Items.Clear();

                if (newvalue == null) return;

                foreach (var item in newvalue)
                {
                    if (string.IsNullOrEmpty(picker.DisplayMember))
                    {
                        picker.Items.Add(item.ToString());
                    }
                    else
                    {
                        picker.Items.Add(picker.DisplayMember);
                    }
                }

            }
        }

        private void OnSelectedIndexChanged(object sender, EventArgs eventArgs)
        {
            if (SelectedIndex < 0 || SelectedIndex > Items.Count - 1)
            {
                SelectedItem = null;
            }
            else
            {
                SelectedItem = ItemsSource[SelectedIndex];  
                
                if(SelectedItem is SideOrderModel)
                {
                    _hardRef.SelectedModel = SelectedItem as SideOrderModel;
                }  
                            
            }
        }

        private static void UpdateSelected(BindableObject bindable, object oldvalue, object newvalue)
        {

            var picker = bindable as BindablePickerControl;
            if (picker != null)
                if (picker.ItemsSource != null)
                    if (picker.ItemsSource.Contains(picker.SelectedItem))
                        picker.SelectedIndex = picker.ItemsSource.IndexOf(picker.SelectedItem);
                    else
                        picker.SelectedIndex = -1;

        }
    }
}
