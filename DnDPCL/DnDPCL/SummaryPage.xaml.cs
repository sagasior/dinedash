﻿using DnDPCL.Controls;
using DnDPCL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DnDPCL
{
    public partial class SummaryPage : ContentPage
    {
        private SessionInfo _session;

        public SummaryPage(SessionInfo ses)
        {
            InitializeComponent();
            _session = ses;
            PopulateControl();
        }

        private void PopulateControl()
        {
            var stkPnl = this.FindByName<StackLayout>("menuItems");
            foreach(DishModel dm in _session.OrderedItem)
            {
                stkPnl.Children.Add(new OrderSummaryControl(dm));
            }
            double subTotal = _session.OrderedItem.Sum(x => x.PriceSum);
            stkPnl.Children.Add(OrderSummaryControl.CreateSubTotalGrid("Total", subTotal));
        }

        void Submit_clicked(object sender, EventArgs e)
        {
            Random rnd = new Random();
            int id = rnd.Next(1, 999);
            DisplayAlert("Order Submitted", String.Format("Enjoy Your Meal. Your order number is {0}",id), "OK");
        }
    }
}
