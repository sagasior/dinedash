﻿using DnDPCL.Controls;
using DnDPCL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DnDPCL
{
    public partial class OrderItemPage : ContentPage
    {
        private DishModel _selectedDishmodel;

        //public OrderItemPage()
        //{
        //    _selectedDishmodel = new DishModel();
        //    Initalize();
        //}

        public OrderItemPage(DishModel model)
        {
            _selectedDishmodel = model;
            Initalize();
        }

        private void Initalize()
        {
            InitializeComponent();
            this.BindingContext = _selectedDishmodel;
            AddIngrdientList();
        }

        public void BtnMinusClicked(object sender, EventArgs e)
        {
            if (_selectedDishmodel.Quantity > 0)
            {
                _selectedDishmodel.Quantity--;
                RemoveFromPanel();
            }          
        }

        public void BtnAddClicked(object sender, EventArgs e)
        {
            _selectedDishmodel.Quantity++; 
            
            if(_selectedDishmodel.ConfigurationModel.Count > 0)
            {
                AddItemToPanel();
            }           
        }

        private void AddItemToPanel()
        {

            var stkPnl = this.FindByName<StackLayout>("ItemsSubMenu");
            string headerString = "Customize Order " + _selectedDishmodel.Quantity;

            //creates shallow copy
            ObservableCollection<OrderConfigurationModel> mdl = new ObservableCollection<OrderConfigurationModel>(_selectedDishmodel.ConfigurationModel);
            _selectedDishmodel.SideOrderItems.Add(mdl);

            stkPnl.Children.Add(new OrderItemSubMenu(mdl, headerString));
        }

        private void AddItemToPanelList()
        {
            _selectedDishmodel.SideOrderItems.Add(new ObservableCollection<OrderConfigurationModel>(_selectedDishmodel.ConfigurationModel));  
        }


        private void AddIngrdientList()
        {
            var stkPnl = this.FindByName<StackLayout>("IngedientsList");

            List<string> strList = new List<string>();
            foreach(Ingredient i in _selectedDishmodel.Ingredients)
            {
                strList.Add(i.Name);
            }
            stkPnl.Children.Add(new TextBindingStackLayout("Ingredient", strList, 12));
        }

        private void RemoveFromPanel()
        {
            var stkPnl = this.FindByName<StackLayout>("ItemsSubMenu");
            if (stkPnl!= null && stkPnl.Children.Count > 0)
            {
                OrderItemSubMenu selectedMenu = stkPnl.Children[stkPnl.Children.Count - 1] as OrderItemSubMenu;
                _selectedDishmodel.SideOrderItems.Remove(selectedMenu.subItems);
                stkPnl.Children.RemoveAt(stkPnl.Children.Count - 1);               
            }
        }

        //validate that all sides are entered before allowing the return

        protected override void OnAppearing()
        {
            base.OnAppearing();      
            
            
            //re-populate the stackpanel with the previous items      
        }


    }
}
