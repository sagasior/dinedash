﻿using DnDPCL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DnDPCL
{
    public partial class LoginPage : ContentPage
    {

        private User LoggedUser;

        private string _userName;
        private string _password;

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public LoginPage()
        {

            InitializeComponent();
            LoggedUser = new User(); 
           
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if(IsUserLoggedIn())
            {
                //Navigation.PushAsync(new TablePage());
            }
            else
            {
                //stay and wait till user presses new user
            }

        }

        private bool IsUserLoggedIn()
        {
            //checks DB to see if user is logged in
            return true;
        }

        protected void CreateUserProfile()
        {
            //query from DB
        }

        void NewUserClicked(object sender, EventArgs e)
        {
            //open the new account page 
            NewUserPage newProfile = new NewUserPage(LoggedUser);            
            Navigation.PushAsync(newProfile);
        }

           
        void LoginClicked(object sender, EventArgs e)
        {
            if(IsUserLoggedIn())
            {
                Navigation.PushAsync(new TablePage());
            }
            else
            {
                DisplayAlert("Authentication Error", "Incorrect Password or Username", "OK");
            }
        }

        
    }
}
