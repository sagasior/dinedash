﻿using DnDPCL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnDPCL.DataBase
{
    public static class DataBaseHelper
    {

        public static ObservableCollection<DishModel> GetApps()
        {
            ObservableCollection<DishModel> dm = new ObservableCollection<DishModel>();

            dm.Add(new DishModel()
            {
                Name = "Bruschetta",
                Description = "A delicious blend of roma tomatoes and shaved grana padano cheese built on top of fresh italian bread",
                Price = 8.50,
                ImageURI = "DnDPCL.Images.bruschetta.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                        new Ingredient("Romaine Lettuce"),
                        new Ingredient("Bacon"),
                        new Ingredient("Parmesan Cheese"),
                        new Ingredient("Garlic Croutons"),
                        new Ingredient("Creamy Garlic Dressing")
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.Islamic,
                            DietRestrictions.Kosher                          
                        }             
            });

            dm.Add(new DishModel()
            {
                Name = "Shrimp Cocktail",
                Description = "Chilled Black Tiger shrimp with a zesty martini cocktail sauce",
                Price = 14.50,
                ImageURI = "DnDPCL.Images.shrimpcocktail.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                        new Ingredient("toasted italian bread"),
                        new Ingredient("chopped roma tomatoes"),
                        new Ingredient("shaved grana padano cheese")

                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.Islamic,
                            DietRestrictions.Kosher
                        }
            });

            dm.Add(new DishModel()
            {
                Name = "Caesar Salad",
                Description = "The best ceaser salad ever!!",
                Price = 10.49,
                ImageURI = "DnDPCL.Images.CeasarSalad.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                        new Ingredient("Romaine Lettuce"),
                        new Ingredient("Bacon"),
                        new Ingredient("Parmesan Cheese"),
                        new Ingredient("Garlic Croutons"),
                        new Ingredient("Creamy Garlic Dressing")
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.Islamic,
                            DietRestrictions.Kosher
                        },
                ConfigurationModel = new ObservableCollection<OrderConfigurationModel>()
                        {
                            new OrderConfigurationModel()
                            {
                                Title = "Add On",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Grilled Chicked", 4.99),
                                    new SideOrderModel("Buffalo Breaded Chicken", 4.99),
                                    new SideOrderModel("Steak", 7.99)
                                }
                            }
                        }
            });

            dm.Add(new DishModel()
            {
                Name = "Classic Nachos",
                Description = "Mile high nachos",
                Price = 15.99,
                ImageURI = "DnDPCL.Images.nachos.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                            new Ingredient("Corn Tortilla Chips"),
                            new Ingredient("Cheddar & Mozzarella Cheese"),
                            new Ingredient("Diced Tomatoes"),
                            new Ingredient("Jalapens"),
                            new Ingredient("Green Onions"),
                            new Ingredient("Black Olives"),
                            new Ingredient("Salsa"),
                            new Ingredient("Sour Cream"),
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.Kosher,
                            DietRestrictions.Islamic
                        },
                ConfigurationModel = new ObservableCollection<OrderConfigurationModel>()
                        {
                            new OrderConfigurationModel()
                            {
                                Title = "Add On",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Grilled Chicked", 4.99),
                                    new SideOrderModel("Buffalo Breaded Chicken", 4.99),
                                    new SideOrderModel("Beef Chili", 3.99),
                                    new SideOrderModel("Guacamole", 2.99),
                                }
                            }
                        }
            });

            dm.Add(new DishModel()
            {
                Name = "Spinach Dip",
                Description = "m blend of spinach, artichoke hearts & cheese, tortilla chips, salsa, sour cream",
                Price = 12.99,
                ImageURI = "DnDPCL.Images.SpinachDip.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                        new Ingredient("Spinach"),
                        new Ingredient("Artichoke Heart"),
                        new Ingredient("Cheese"),
                        new Ingredient("Tortilla Chips"),
                        new Ingredient("Salsa"),
                        new Ingredient("Sour Cream")
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.None                            
                        }
            });

            dm.Add(new DishModel()
            {
                Name = "Greek Salad",
                Description = "The best greek salad ever!!",
                Price = 10.49,
                ImageURI = "DnDPCL.Images.GreekSalad.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                            new Ingredient("Romaine Lettuce"),
                            new Ingredient("Feta Cheese"),
                            new Ingredient("Black Olives"),
                            new Ingredient("Tomatos"),
                            new Ingredient("Onions"),
                            new Ingredient("Black Olives"),
                            new Ingredient("Greek Dressing")
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.Islamic,
                            DietRestrictions.Kosher
                        },
                ConfigurationModel = new ObservableCollection<OrderConfigurationModel>()
                        {
                            new OrderConfigurationModel()
                            {
                                Title = "Add On",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Grilled Chicked", 4.99),
                                    new SideOrderModel("Buffalo Breaded Chicken", 4.99),
                                    new SideOrderModel("Steak", 7.99)
                                }
                            }
                        }
            });

            dm.Add(new DishModel()
            {
                Name = "Greek French Fries",
                Description = "If you like fries and feta cheese you need to try this",
                Price = 12.50,
                ImageURI = "DnDPCL.Images.fries.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                            new Ingredient("French Fries"),
                            new Ingredient("Feta Cheese"),
                            new Ingredient("Tomato Based Sauce")
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.None
                        }
            });

            dm.Add(new DishModel()
            {
                Name = "Cheese Platter",
                Description = "A nice mix of cheese and fresh breads to get you ready for your meal",
                Price = 12.50,
                ImageURI = "DnDPCL.Images.cheesePlate.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                            new Ingredient("Cheddar Cheese"),
                            new Ingredient("Feta Cheese"),
                            new Ingredient("brie cheese"),
                            new Ingredient("mozzarella"),
                            new Ingredient("Gouda")
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.None
                        }
            });

            return dm;
        }

        public static ObservableCollection<DishModel> GetEntres()
        {

            ObservableCollection<DishModel> dm = new ObservableCollection<DishModel>();

            dm.Add(new DishModel()
                    {
                        Name = "Pizza Rustica",
                        Description = "Homemade pizza dough topped gorgonzola cheese, mozzarella cheese, spicy sausage, rapini",
                        Price = 16.99,                       
                        ImageURI = "DnDPCL.Images.pizzarustica.png",
                        Ingredients = new ObservableCollection<Ingredient>()
                        {
                            new Ingredient("Gorgonzola Cheese"),
                            new Ingredient("Mozzarella Cheese"),
                            new Ingredient("Spicy Sausage"),
                            new Ingredient("Rapini")
                        },
                        DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.Kosher,
                            DietRestrictions.Islamic
                        }
                    });

            dm.Add(new DishModel()
            {
                Name = "Prime Rib",
                Description = "Aged for extra flavour and tenderness, rubbed with our unique blend of spices and slow roasted. Hand-carved and served with horseradish and red wine herb jus.  Served with a ceaser salad, sauteed mushrooms ",
                Price = 32.50,
                ImageURI = "DnDPCL.Images.primerib.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                        new Ingredient("Prime Rib Beef"),
                        new Ingredient("Lettuce"),
                        new Ingredient("Croutons"),
                        new Ingredient("Paramsian Cheese"),
                        new Ingredient("Ceaser Dressing"),
                        new Ingredient("Mushrooms"),
                        new Ingredient("Potatoes"),
                        new Ingredient("Milk"),
                        new Ingredient("Garlic"),
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.Kosher,
                            DietRestrictions.Islamic
                        },
                ConfigurationModel = new ObservableCollection<OrderConfigurationModel>()
                        {
                            new OrderConfigurationModel()
                            {
                                Title = "Salad",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Garden", 0),
                                    new SideOrderModel("Caesar", 2.00),
                                    new SideOrderModel("Greek", 0),
                                    new SideOrderModel("Coleslaw", 0)
                                }
                            },
                            new OrderConfigurationModel()
                            {
                                Title = "Sides",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Baked Potato", 0),
                                    new SideOrderModel("Mashed Potato", 2.00),
                                    new SideOrderModel("French Fries", 0),
                                    new SideOrderModel("Rice", 0),
                                    new SideOrderModel("Steamed Vegetables", 0)
                                }
                            },
                            new OrderConfigurationModel()
                            {
                                Title = "Meat Cooked",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Rare", 0),
                                    new SideOrderModel("Medium Rare", 0),
                                    new SideOrderModel("Medium", 0),
                                    new SideOrderModel("Medium Well", 0),
                                    new SideOrderModel("Well", 0)
                                }
                            }
                        }
            });

            dm.Add(new DishModel()
            {
                Name = "Pistachio Crusted Salmon",
                Description = "Served on garlic mashed potato, bacon sautéed Brussels sprouts, and maple butter.",
                Price = 27.50,
                ImageURI = "DnDPCL.Images.salmon.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                            new Ingredient("Potatoes,"),
                            new Ingredient("Milk"),
                            new Ingredient("Crushed Garlic"),
                            new Ingredient("Bacon"),
                            new Ingredient("Brussels,"),
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.Kosher,
                            DietRestrictions.Islamic
                        },
                ConfigurationModel = new ObservableCollection<OrderConfigurationModel>()
                        {
                            new OrderConfigurationModel()
                            {
                                Title = "Salad",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Garden", 0),
                                    new SideOrderModel("Caesar", 2.00),
                                    new SideOrderModel("Greek", 0),
                                    new SideOrderModel("Coleslaw", 0)
                                }
                            },
                            new OrderConfigurationModel()
                            {
                                Title = "Sides",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Baked Potato", 0),
                                    new SideOrderModel("Mashed Potato", 2.00),
                                    new SideOrderModel("French Fries", 0),
                                    new SideOrderModel("Rice", 0),
                                    new SideOrderModel("Steamed Vegetables", 0)
                                }
                            }    
                        }
            });

            dm.Add(new DishModel()
            {
                Name = "Fajitas",
                Description = "1 lb of chicken pilled high on sauteed vegetables for you to enjoy on your own or to share as a table",
                Price = 18.49,
                ImageURI = "DnDPCL.Images.chickenfajitas.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                        new Ingredient("Sauteed Onions"),
                        new Ingredient("Sweet Peppers"),
                        new Ingredient("Diced Tomatoes"),
                        new Ingredient("Shredded Lettuce"),
                        new Ingredient("Cheddar & Mozzerella Cheese"),
                        new Ingredient("Sour Cream"),
                        new Ingredient("Pico de Gallo"),
                        new Ingredient("Guacamole"),
                        new Ingredient("Flour Tortilla Shells"),
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.Kosher,
                            DietRestrictions.Islamic
                        },
                ConfigurationModel = new ObservableCollection<OrderConfigurationModel>()
                        {
                            new OrderConfigurationModel()
                            {
                                Title = "Meat Type",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Chicken", 5.50),
                                    new SideOrderModel("Steak", 7.00),
                                    new SideOrderModel("Shrimp", 6.00)                                  
                                }
                            },
                            new OrderConfigurationModel()
                            {
                                Title = "Sides",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {               
                                    new SideOrderModel("Refried Beans", 0),
                                    new SideOrderModel("Rice", 0),
                                    new SideOrderModel("Fries", 0)
                                }
                            },
                            new OrderConfigurationModel()
                            {
                                Title = "Meat Cooked",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Rare", 0),
                                    new SideOrderModel("Medium Rare", 0),
                                    new SideOrderModel("Medium", 0),
                                    new SideOrderModel("Medium Well", 0),
                                    new SideOrderModel("Well", 0)
                                }
                            }
                        }
            });

            dm.Add(new DishModel()
            {
                Name = "Southwest Burger",
                Description = "Our homemade beef patty piled high with fresh ingredients and a delicious homemade bun",
                Price = 18.49,
                ImageURI = "DnDPCL.Images.southwestburger.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                            new Ingredient("8oz beef patty"),
                            new Ingredient("bacon"),
                            new Ingredient("swiss cheese"),
                            new Ingredient("smokey bbq sauce"),
                            new Ingredient("crispy onions")   
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.Kosher,
                            DietRestrictions.Islamic
                        },
                ConfigurationModel = new ObservableCollection<OrderConfigurationModel>()
                        {

                             new OrderConfigurationModel()
                            {
                                Title = "Salad",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Garden", 0),
                                    new SideOrderModel("Caesar", 0),
                                    new SideOrderModel("Greek", 0),
                                    new SideOrderModel("Coleslaw", 0)
                                }
                            },
                            new OrderConfigurationModel()
                            {
                                Title = "Sides",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Baked Potato", 0),
                                    new SideOrderModel("Mashed Potato",0),
                                    new SideOrderModel("French Fries", 0),
                                    new SideOrderModel("Rice", 0),
                                    new SideOrderModel("Steamed Vegetables", 0)
                                }
                            },
                            new OrderConfigurationModel()
                            {
                                Title = "Meat Cooked",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Rare", 0),
                                    new SideOrderModel("Medium Rare", 0),
                                    new SideOrderModel("Medium", 0),
                                    new SideOrderModel("Medium Well", 0),
                                    new SideOrderModel("Well", 0)
                                }
                            }
                        }
            });

            dm.Add(new DishModel()
            {
                Name = "New York Steak Dinner",
                Description = "Aged for extra flavour and tenderness, rubbed with our unique blend of spices and slow roasted. Served with a ceaser salad, sauteed mushrooms and garlic mashed potatos",
                Price = 29.99,
                ImageURI = "DnDPCL.Images.steak.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                            new Ingredient("12 oz New York steak"),
                            new Ingredient("Lettuce,"),
                            new Ingredient("Croutons"),
                            new Ingredient("Paramsian Cheese"),
                            new Ingredient("Ceaser Dressing"),
                            new Ingredient("Mushrooms,"),
                            new Ingredient("Potatoes,"),
                            new Ingredient("Crushed Garlic,")
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.Kosher,
                            DietRestrictions.Islamic
                        },
                ConfigurationModel = new ObservableCollection<OrderConfigurationModel>()
                        {

                             new OrderConfigurationModel()
                            {
                                Title = "Salad",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Garden", 0),
                                    new SideOrderModel("Caesar", 0),
                                    new SideOrderModel("Greek", 0),
                                    new SideOrderModel("Coleslaw", 0)
                                }
                            },
                            new OrderConfigurationModel()
                            {
                                Title = "Sides",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Baked Potato", 0),
                                    new SideOrderModel("Mashed Potato",0),
                                    new SideOrderModel("French Fries", 0),
                                    new SideOrderModel("Rice", 0),
                                    new SideOrderModel("Steamed Vegetables", 0)
                                }
                            },
                            new OrderConfigurationModel()
                            {
                                Title = "Meat Cooked",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Rare", 0),
                                    new SideOrderModel("Medium Rare", 0),
                                    new SideOrderModel("Medium", 0),
                                    new SideOrderModel("Medium Well", 0),
                                    new SideOrderModel("Well", 0)
                                }
                            }
                        }
            });

            dm.Add(new DishModel()
            {
                Name = "Chicken Parmesan",
                Description = "Served with penne, our signature tomato sauce and a side salad.",
                Price = 21.99,
                ImageURI = "DnDPCL.Images.ChickenParmesan.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                            new Ingredient("Chicken"),
                            new Ingredient("breading"),
                            new Ingredient("Cheese"),
                            new Ingredient("Penne Pasta"),
                            new Ingredient("Homemade Tomato Sauce")      
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.Kosher,
                            DietRestrictions.Islamic
                        },
                ConfigurationModel = new ObservableCollection<OrderConfigurationModel>()
                        {

                             new OrderConfigurationModel()
                            {
                                Title = "Salad",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Garden", 0),
                                    new SideOrderModel("Caesar", 0),
                                    new SideOrderModel("Greek", 0),
                                    new SideOrderModel("Coleslaw", 0)
                                }
                            },
                            new OrderConfigurationModel()
                            {
                                Title = "Meat Type",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Chicken", 0),
                                    new SideOrderModel("Veal", 2.00)                                   
                                }
                            }                            
                        }
            });

            dm.Add(new DishModel()
            {
                Name = "Club Sandwich",
                Description = "A triple-decker treat of Butterball deli-style roast turkey, applewood smoked bacon, lettuce and tomatoes on a giant onion bun. Served with a pickle spear and our famous house-made coleslaw.",
                Price = 14.99,
                ImageURI = "DnDPCL.Images.clubSand.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                            new Ingredient("Butterball Roast Turkey"),
                            new Ingredient("Applewood Smoked Bacon"),
                            new Ingredient("Lettuce"),
                            new Ingredient("tomatoes")
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.Kosher,
                            DietRestrictions.Islamic
                        },
                ConfigurationModel = new ObservableCollection<OrderConfigurationModel>()
                        {

                             new OrderConfigurationModel()
                            {
                                Title = "Bread Type",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Onion Bun", 0),
                                    new SideOrderModel("White", 0),
                                    new SideOrderModel("Brown", 0),
                                    new SideOrderModel("Rye", 0)
                                }
                            },
                             new OrderConfigurationModel()
                            {
                                Title = "Sides",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Baked Potato", 0),
                                    new SideOrderModel("Mashed Potato",0),
                                    new SideOrderModel("French Fries", 0),
                                    new SideOrderModel("Rice", 0),
                                    new SideOrderModel("Steamed Vegetables", 0)
                                }
                            }
                        }
            });
            
            return dm;
        }

        public static ObservableCollection<DishModel> GetDessert()
        {

            ObservableCollection<DishModel> dm = new ObservableCollection<DishModel>();

            dm.Add(new DishModel()
            {
                Name = "Brownie Sundae",
                Description = "A chocolate brownie with vanilla ice cream, hot fudge, caramel and whipped cream.",
                Price = 8.50,
                ImageURI = "DnDPCL.Images.UltimateBrownieSundae.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                        new Ingredient("Brownie"),
                        new Ingredient("Vanilla Icecream"),
                        new Ingredient("Fudge,"),
                        new Ingredient("Caramel,"),
                        new Ingredient("Whipped Cream")
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.None
                        }
            });

            dm.Add(new DishModel()
            {
                Name = "Caramel Fudge Brownie Cheesecake",
                Description = "chocolate brownie, smothered with caramel on vanilla cheesecake with chocolate brownie chunks inside and iced with chocolate ganache, layered with caramel additional butter caramel sauce on top",
                Price = 7.25,
                ImageURI = "DnDPCL.Images.CaramelFudgeBrownieCheesecake.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                        new Ingredient("chocolate brownie"),
                        new Ingredient("caramel,"),
                        new Ingredient("chocolate ganache"),
                        new Ingredient("butter caramel sauce on top")        
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.None                            
                        }
            });

            dm.Add(new DishModel()
            {
                Name = "Apple Crumble Waffle",
                Description = "White choclate silk ice cream, warm oven-baked apple crisp, butter caramel sauce, cinnamon streusel",
                Price = 10.49,
                ImageURI = "DnDPCL.Images.applecrumblewaffle.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                        new Ingredient("White choclate silk ice cream"),
                        new Ingredient("Apple Crisp"),
                        new Ingredient("Butter Caramel Sauce"),
                        new Ingredient("Cinnamon Streusel")
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.None
                        }               
            });

            dm.Add(new DishModel()
            {
                Name = "Nutella Milkshake",
                Description = "The title says it all",
                Price = 7.99,
                ImageURI = "DnDPCL.Images.NutellaMilkshake.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                            new Ingredient("Nutella,"),
                            new Ingredient("Chocolate Ice Cream"),
                            new Ingredient("Chocolate Sauce"),
                            new Ingredient("Whipped Cream"),
                            new Ingredient("Chocolate Milk")
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.None
                        }                
            });

            dm.Add(new DishModel()
            {
                Name = "Banana Split",
                Description = "chocalate ice cream, honey vanilla ice cream, strawberry royale ice cream, sliced banana, fresh strawberries, belgain milk chocolate sauce, butter caramel sauce",
                Price = 12.99,
                ImageURI = "DnDPCL.Images.bananasplit.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                        new Ingredient("Chocalate Ice Cream"),
                        new Ingredient("Honey Vanilla Ice Cream"),
                        new Ingredient("Strawberry Royale Ice Cream,"),
                        new Ingredient("Strawberries"),
                        new Ingredient("Belgain Milk Chocolate Sauce"),
                        new Ingredient("Butter Caramel Sauce")
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.None
                        }
            });

            dm.Add(new DishModel()
            {
                Name = "Homemade Tiramisu",
                Description = "With shaved dark chocolate, espresso dipped ladyfingers, real whipped cream, caramel and strawberry wedges. ",
                Price = 7.49,
                ImageURI = "DnDPCL.Images.tiramisu.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                            new Ingredient("Dark Chocolate"),
                            new Ingredient("Espresso Dipped Ladyfingers"),
                            new Ingredient("Real Whipped Cream"),
                            new Ingredient("Caramel"),
                            new Ingredient("Strawberry Wedges")
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.None
                        }               
            });

            dm.Add(new DishModel()
            {
                Name = "Jumbo Skor Sundae",
                Description = "Chunks of delicious SKOR candy bar mixed with caramel, vanilla yogurt and topped with whipped cream and chocolate sauce. ",
                Price = 12.50,
                ImageURI = "DnDPCL.Images.UltimateBrownieSundae.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                            new Ingredient("SKOR Candy Bar"),
                            new Ingredient("Vanilla Yogurt"),
                            new Ingredient("Whipped Cream"),
                            new Ingredient("Chocolate Sauce")
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.None
                        }
            });

            dm.Add(new DishModel()
            {
                Name = "Cheesecake",
                Description = "Choice of topping: blueberry, strawberry or cherry. ",
                Price = 8.50,
                ImageURI = "DnDPCL.Images.Cheesecake.png",
                Ingredients = new ObservableCollection<Ingredient>()
                        {
                            new Ingredient("Cheddar Cheese"),
                            new Ingredient("Feta Cheese"),
                            new Ingredient("brie cheese"),
                            new Ingredient("mozzarella"),
                            new Ingredient("Gouda")
                        },
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.None
                        },
                ConfigurationModel = new ObservableCollection<OrderConfigurationModel>()
                        {

                             new OrderConfigurationModel()
                            {
                                Title = "Toppigns",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Blueberry", 0),
                                    new SideOrderModel("Strawberry", 0),
                                    new SideOrderModel("Cherry", 0)
                                }
                            }
                        }
            });

            return dm;

        }

        public static ObservableCollection<DishModel> GetDrinks()
        {

            ObservableCollection<DishModel> dm = new ObservableCollection<DishModel>();

            dm.Add(new DishModel()
            {
                Name = "Soda",
                Description = "Free Refills",
                Price = 1.50,
                ImageURI = "DnDPCL.Images.pop.png",                
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.None
                        },
                ConfigurationModel = new ObservableCollection<OrderConfigurationModel>()
                        {

                             new OrderConfigurationModel()
                            {
                                Title = "Type",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Coke", 0),
                                    new SideOrderModel("Sprite", 0),
                                    new SideOrderModel("Root Beer", 0),
                                    new SideOrderModel("Iced Tea", 0),
                                    new SideOrderModel("Canada Dry", 0)
                                }
                            }
                        }

            });

            dm.Add(new DishModel()
            {
                Name = "Water",
                Description = "Free Refills",
                Price = 0.00,
                ImageURI = "DnDPCL.Images.cupwater.png",
                DishRestrictions = new ObservableCollection<DietRestrictions>()
                        {
                            DietRestrictions.None
                        },
                ConfigurationModel = new ObservableCollection<OrderConfigurationModel>()
                        {

                             new OrderConfigurationModel()
                            {
                                Title = "Addition",
                                SubMenuItems = new ObservableCollection<SideOrderModel>()
                                {
                                    new SideOrderModel("Lemon", 0),
                                    new SideOrderModel("Lime", 0)
                                }
                            }
                        }
            });

            return dm;

        }



    }

    





    //dm.Add(new DishModel()
    //{
    //    Name = "Dish3",
    //            Description = "yyayaya",
    //            Price = 10.99,
    //            ImageURI = "DnDPCL.Images.dish.png",
    //            Ingredients = new ObservableCollection<Ingredient>()
    //                    {
    //                    new Ingredient("temp"),
    //                    new Ingredient("temp2"),
    //                    new Ingredient("temp3"),
    //                    new Ingredient("temp4"),
    //                    new Ingredient("temp5")
    //                    },
    //            DishRestrictions = new ObservableCollection<DietRestrictions>()
    //                    {
    //                        DietRestrictions.None,
    //                        DietRestrictions.Vegan,
    //                        DietRestrictions.Islamic
    //                    }
    //        });

    //        dm.Add(new DishModel()
    //{
    //    Name = "Dish1",
    //            Description = "yyayaya",
    //            Price = 10.99,
    //            ImageURI = "DnDPCL.Images.dish.png",
    //            Ingredients = new ObservableCollection<Ingredient>()
    //                    {
    //                    new Ingredient("temp"),
    //                    new Ingredient("temp2"),
    //                    new Ingredient("temp3"),
    //                    new Ingredient("temp4"),
    //                    new Ingredient("temp5")
    //                    },
    //            DishRestrictions = new ObservableCollection<DietRestrictions>()
    //                    {
    //                        DietRestrictions.None,
    //                        DietRestrictions.Vegan,
    //                        DietRestrictions.Islamic
    //                    }
    //        });

    //        dm.Add(new DishModel()
    //{
    //    Name = "Dish1",
    //            Description = "yyayaya",
    //            Price = 10.99,
    //            ImageURI = "DnDPCL.Images.dish.png",
    //            Ingredients = new ObservableCollection<Ingredient>()
    //                    {
    //                    new Ingredient("temp"),
    //                    new Ingredient("temp2"),
    //                    new Ingredient("temp3"),
    //                    new Ingredient("temp4"),
    //                    new Ingredient("temp5")
    //                    },
    //            DishRestrictions = new ObservableCollection<DietRestrictions>()
    //                    {
    //                        DietRestrictions.None,
    //                        DietRestrictions.Vegan,
    //                        DietRestrictions.Islamic
    //                    }
    //        });


}
