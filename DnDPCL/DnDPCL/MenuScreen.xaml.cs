﻿using DnDPCL.DataBase;
using DnDPCL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DnDPCL
{
    public partial class MenuScreen : ContentPage
    {
        private SubMenu _subMenu;
        private SessionInfo _session;

        public ObservableCollection<DishModel> _dishModelList;

        public ObservableCollection<DishModel> DishModelCollection
        {
            get { return _dishModelList; }
            set { _dishModelList = value; }
        }


        public SubMenu SubMenu
        {
            get { return _subMenu; }
            set { _subMenu = value; }
        }

        public MenuScreen(SubMenu menu, SessionInfo session)
        {
            _subMenu = menu;
            _session = session;
            InitializeComponent();
            Initalize();
        }

        private void Initalize()
        {
            DishModelCollection = _subMenu.Dishs;
            this.Title = _subMenu.Title;
            BindingContext = this;           
        }


        void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }

            if(e.SelectedItem is DishModel)
            {
                DishModel dm = e.SelectedItem as DishModel;
                _session.OrderedItem.Add(dm);
                Navigation.PushAsync(new OrderItemPage(dm));
            }            
        }

        void SubmitButtonClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new SummaryPage(_session));
        }

    }
}
