﻿using DnDPCL.DataBase;
using DnDPCL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DnDPCL
{
    public partial class TabbedMenuScreen : TabbedPage
    {

        private SessionInfo _session;

        public TabbedMenuScreen()
        {
            InitializeComponent();

            _session = new SessionInfo();                        
            SubMenu Aps = new SubMenu()
            {
                Title = "Apps",
                Dishs = DataBaseHelper.GetApps()
            };

            SubMenu ents = new SubMenu()
            {
                Title = "Mains",
                Dishs = DataBaseHelper.GetEntres()
            };

            SubMenu desrt = new SubMenu()
            {
                Title = "Dessert",
                Dishs = DataBaseHelper.GetDessert()
            };

            SubMenu drinks = new SubMenu()
            {
                Title = "Drinks",
                Dishs = DataBaseHelper.GetDrinks()
            };            

            Children.Add(new MenuScreen(Aps, _session));
            Children.Add(new MenuScreen(ents, _session));
            Children.Add(new MenuScreen(desrt, _session));
            Children.Add(new MenuScreen(drinks, _session));
        }
    }
}
